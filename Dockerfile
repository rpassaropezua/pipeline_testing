FROM golang:latest

RUN go get -d -v github.com/go-sql-driver/mysql
RUN go get -d -v github.com/gorilla/mux
RUN go get -d -v github.com/jmoiron/sqlx
RUN go get -d -v github.com/google/uuid
RUN go get github.com/satori/go.uuid
RUN go get github.com/enbritely/heartbeat-golang


RUN mkdir /app
ADD . /app/
WORKDIR /app
RUN go build -o main .
EXPOSE 8000

CMD ["/app/main"]

#ENTRYPOINT [ "mresencerestservice" ]