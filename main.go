package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
)

type Credentials struct {
	Cid     string `json:"clientID"`
	Csecret string `json:"clientSecret"`
}

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/hello", sayHello).Methods("GET")
	router.HandleFunc("/heartbeat", heartbeat).Methods("GET")
	log.Fatal(http.ListenAndServe(":8000", router))
}

func initGoogleAuth() {
	var c Credentials
	file, err := ioutil.ReadFile("./test-Oauth-creds.json")
	if err != nil {
		log.Fatal(err)
	}

	json.Unmarshal(file, &c)
}

func heartbeat(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func sayHello(w http.ResponseWriter, r *http.Request) {
	type message struct {
		Content string
	}
	msg := message{"hello world"}

	resp, err := json.Marshal(msg.Content)
	if err != nil {
		panic(err)
	}

	w.WriteHeader(http.StatusOK)
	w.Write(resp)
}
